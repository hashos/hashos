#!/bin/bash

if [[ "$0" != *"bash"* ]]; then
        echo "Make sure you use this script using source!"
        echo "Usage: . load-env.sh or source load-env.sh"
fi

set +h
umask 022

LFS=$(pwd)/mnt
LC_ALL=POSIX
LFS_TGT=$(uname -m)-lfs-linux-gnueabihf
PATH=/usr/bin
if [ ! -L /bin ]; then PATH=/bin:$PATH; fi
PATH=$LFS/tools/bin:$PATH

export LFS LC_ALL LFS_TGT PATH
