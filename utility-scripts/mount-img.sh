#!/bin/bash
rootpart="p2"
bootpart="p1"

if [ -n "$(losetup -a | grep $1)" ]; then
        device=$(losetup --list | grep "/backup/HashOS-images/HashOS-0.1.img" | awk '{ print $1 }')
        rootmountpoint=$(mount -l | grep $device$rootpart | awk '{ print $3 }')
        bootmountpoint=$(mount -l | grep $device$bootpart | awk '{ print $3 }')
 
        if [ -n "$bootmountpoint" ]; then
                echo "Unmounting previous boot loopback partition"
                umount $bootmountpoint
        fi

        if [ -n "$rootmountpoint" ]; then
                echo "Unmounting previous loopback device"
                umount $rootmountpoint
        fi

        echo "Removing previous loopback device"
        
        losetup --detach $device
fi

losetup -fP --show $1
device=$(losetup --list | grep "/backup/HashOS-images/HashOS-0.1.img" | awk '{ print $1 }')

mount -o defaults $device$rootpart $3 && \
mkdir -p $3/boot && \
mount -o umask=0007,gid=$2,uid=$2 $device$bootpart $3/boot
