# HashOS
A lightweight custom OS currently being build for the ARM platform.
Goals are to better understand the linux environment and create an easily reproducible installation experience across Raspberry Pi devices, and evenually x86, maybe.

## Tutorial of sorts
This was meant to be built on a Pi, going to look into cross compilation support later on, would speed things up tremendously.

### Target SD card formatting
Format an SD card such that it has at least a fat32 boot partition and a ext4 root partition.
If possible add a swap parition, but it's up to you.

### Mounting the SD card and loading the environment
Create the directory `mnt` in the root of the project, then mount the root partition created above to this folder, and the boot partition to `mnt/boot`.
Now that everything is mounted run the `load-env.sh` script in the `utility-scripts` directory.
Make sure you run it as such `. ./utility-scripts/load-env.sh`, the leading `.` ensures that the script is run in the calling shell environment and as such all variables set up here persist.
It is also imperative that you run this script in the same director that the mnt directory resides.

### Basic install scripts
The basic install scripts will create a bootable SD card.
For the sake of avoiding repeating long computing tasks creating the bootable SD card is split into multiple parts.

#### Part 1
This will download all sources to the SD card and create the needed directories for the install.
This may take some time, and the sources downloaded from LFS's servers might take a bit to start downloading.
Once the sources are done downloading then all of the archives will be extracted so they can be easily worked on in the following parts.

```
./install-scripts/basic-install-1.sh -d
```

In order to start run the `basic-install-1.sh` script in the `install-scripts` directory.
If running for the first time make sure to append the `-d` flag which will download the sources.
This flag exists if for whatever reason you want to obtain a clean extract of all sources then you can just run the script without the `-d`.

#### Part 2
Once all sources are downloaded then the fun part begins.
Running the `basic-install-2.sh` script will start compiling the base project, running sanity checks, and patching code for ARM.
Make sure you have the environment variables loaded using the `load-env.sh` script or else things will just not run.
