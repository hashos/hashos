#!/bin/bash

time ( # just time how long the script takes to run and print it once done

###############################################################################

# if an error occurs exit the script
set -e
NUMTHREADS=4
SLEEPTIME=5

if [[ "$USER" == "root" ]]; then
        echo "Cannot be run as root!"
        exit
fi

if [[ -z $LFS ]]; then
        echo "Load the env!"
        exit
fi

###################
# M4 Installation #
###################

(
        exec > >(sed 's/^/M4: /')
        exec 2> >(sed 's/^/ERROR: /')
        echo "Starting M4 compilation"

        cd $LFS/sources/
        cd $(ls -d1 */ | grep m4)

        sed -i 's/IO_ftrylockfile/IO_EOF_SEEN/' lib/*.c
        echo "#define _IO_IN_BACKUP 0x100" >> lib/stdio-impl.h

        mkdir -pv build
        cd build

        ../configure --prefix=/usr \
                --host=$LFS_TGT \
                --build=$(../build-aux/config.guess)

        make -j $NUMTHREADS 
        make DESTDIR=$LFS install

        echo "Finished M4 compilation"
        sync
        sleep $SLEEPTIME
)

########################
# Ncurses Installation #
########################

(
        exec > >(sed 's/^/Ncurses: /')
        exec 2> >(sed 's/^/ERROR: /')
        echo "Starting Ncurses compilation"

        cd $LFS/sources/
        cd $(ls -d1 */ | grep ncurses)

        # normally the configure script runs through different awk interpreters
        # until it finds a suitable one, since we want to use the gawk interpreter
        # we remove mawk from the list of potential interpreters.
        # the list usually goes: mawk -> gawk -> nawk -> awk
        sed -i s/mawk// configure

        # build sub-program tic
        mkdir -pv build-tic
        cd build-tic
        
        ../configure

        make -j $NUMTHREADS -C include
        make -j $NUMTHREADS -C progs tic

        cd ..


        mkdir -pv build-ncurses
        cd build-ncurses

        ../configure --prefix=/usr \
                --host=$LFS_TGT \
                --build=$(../config.guess) \
                --mandir=/usr/share/man \
                --with-manpage-format=normal \
                --with-shared \
                --without-debug \
                --without-ada \
                --without-normal \
                --enable-widec

        make -j $NUMTHREADS
        make DESTDIR=$LFS TIC_PATH=$(cd ..; pwd)/build-tic/progs/tic install

        cd ..

        # small linker script since programs rely on ncurses
        echo "INPUT(-lncursesw)" > $LFS/usr/lib/libncurses.so

        # move shared libraries to /lib since we expect them there
        mv -v $LFS/usr/lib/libncursesw.so.6* $LFS/lib

        # however some programs might still look to /usr/lib for ncurses, so we're
        # creating a symlink from the /usr/lib libraries to the /lib libraries
        ln -sfv ../../lib/$(readlink $LFS/usr/lib/libncursesw.so) $LFS/usr/lib/libncursesw.so

        echo "Finished Ncurses compilation"
        sync
        sleep $SLEEPTIME
)

#####################
# Bash Installation #
#####################

(
        exec > >(sed 's/^/Bash: /')
        exec 2> >(sed 's/^/ERROR: /')
        echo "Starting Bash compilation"

        cd $LFS/sources/
        cd $(ls -d1 */ | grep bash)

        mkdir -pv build
        cd build

        ../configure --prefix=/usr \
                --build=$(../support/config.guess) \
                --host=$LFS_TGT \
                --without-bash-malloc

        make -j $NUMTHREADS
        make DESTDIR=$LFS install

        # move to expected location
        mv $LFS/usr/bin/bash $LFS/bin/bash
        
        # link sh to bash for programs that use sh
        # works since there is a bash executable in $LFS/bin
        ln -sfv bash $LFS/bin/sh

        echo "Finished Bash compilation"
        sync
        sleep $SLEEPTIME
)

##########################
# Coreutils Installation #
##########################

(
        exec > >(sed 's/^/Coreutils: /')
        exec 2> >(sed 's/^/ERROR: /')
        echo "Starting Coreutils compilation"

        cd $LFS/sources/
        cd $(ls -d1 */ | grep coreutils)

        # Apply patch
        patch -Np1 -i ../$(cd ../; ls -1 | grep -E "coreutils-.*-getdents\.patch") || true

        mkdir -pv build
        cd build

        ../configure --prefix=/usr \
                --host=$LFS_TGT \
                --build=$(../build-aux/config.guess) \
                --enable-install-program=hostname \
                --enable-no-install-program=kill,uptime

        make -j $NUMTHREADS
        make DESTDIR=$LFS install

        # move programs to their final expected locations
        mv -v $LFS/usr/bin/{cat,chgrp,chmod,chown,cp,date,dd,df,echo} $LFS/bin
        mv -v $LFS/usr/bin/{false,ln,ls,mkdir,mknod,mv,pwd,rm}        $LFS/bin
        mv -v $LFS/usr/bin/{rmdir,stty,sync,true,uname}               $LFS/bin
        mv -v $LFS/usr/bin/{head,nice,sleep,touch}                    $LFS/bin
        mv -v $LFS/usr/bin/chroot                                     $LFS/usr/sbin
        mkdir -pv $LFS/usr/share/man/man8
        mv -v $LFS/usr/share/man/man1/chroot.1                        $LFS/usr/share/man/man8/chroot.8
        sed -i 's/"1"/"8"/'                                           $LFS/usr/share/man/man8/chroot.8

        echo "Finished Coreutils compilation"
        sync
        sleep $SLEEPTIME
)

##########################
# Diffutils Installation #
##########################

(
        exec > >(sed 's/^/diffutils: /')
        exec 2> >(sed 's/^/ERROR: /')
        echo "Starting Diffutils compilation"

        cd $LFS/sources/
        cd $(ls -d1 */ | grep diffutils)

        mkdir -pv build
        cd build

        ../configure --prefix=/usr --host=$LFS_TGT

        make -j $NUMTHREADS
        make DESTDIR=$LFS install

        echo "Finished Diffutils compilation"
        sync
        sleep $SLEEPTIME
)

#####################
# File Installation #
#####################

(
        exec > >(sed 's/^/file: /')
        exec 2> >(sed 's/^/ERROR: /')
        echo "Starting file compiliation"

        cd $LFS/sources/
        cd $(ls -d1 */ | grep file)

        mkdir -pv build
        cd build

        ../configure --prefix=/usr \
                     --host=$LFS_TGT

        make -j $NUMTHREADS
        make DESTDIR=$LFS install

        echo "Finished file compilation"
        sync
        sleep $SLEEPTIME
)

##########################
# Findutils Installation #
##########################

(
        exec > >(sed 's/^/findutils: /')
        exec 2> >(sed 's/^/ERROR: /')
        echo "Starting findutils compilation"

        cd $LFS/sources/
        cd $(ls -d1 */ | grep findutils)

        mkdir -pv build
        cd build

        ../configure --prefix=/usr \
                     --host=$LFS_TGT \
                     --build=$(../build-aux/config.guess)

        make -j $NUMTHREADS
        make DESTDIR=$LFS install

        # moving exec to final expected location
        mv -v $LFS/usr/bin/find $LFS/bin
        sed -i 's|find:=${BINDIR}|find:=/bin|' $LFS/usr/bin/updatedb

        echo "Finished findutils compilation"
        sync
        sleep $SLEEPTIME
)

#####################
# Gawk Installation #
#####################

(
        exec > >(sed 's/^/gawk: /')
        exec 2> >(sed 's/^/ERROR: /')
        echo "Starting gawk installation"

        cd $LFS/sources/
        cd $(ls -d1 */ | grep gawk)

        sed -i 's/extras//' Makefile.in

        mkdir -pv build
        cd build

        ../configure --prefix=/usr \
                     --host=$LFS_TGT \
                     --build=$(../config.guess)

        make -j $NUMTHREADS
        make DESTDIR=$LFS install

        echo "Finished gawk installation"
        sync
        sleep $SLEEPTIME
)

#####################
# Grep Installation #
#####################

(
        exec > >(sed 's/^/grep: /')
        exec 2> >(sed 's/^/ERROR: /')
        echo "Starting grep installation"

        cd $LFS/sources/
        cd $(ls -d1 */ | grep grep)

        mkdir -pv build
        cd build

        ../configure --prefix=/usr \
                     --host=$LFS_TGT \
                     --bindir=/bin

        make -j $NUMTHREADS
        make DESTDIR=$LFS install

        echo "Finished grep installation"
        sync
        sleep $SLEEPTIME
)

#####################
# Gzip Installation #
#####################

(
        exec > >(sed 's/^/gzip: /')
        exec 2> >(sed 's/^/ERROR: /')
        echo "Starting gzip installation"

        cd $LFS/sources/
        cd $(ls -d1 */ | grep gzip)

        mkdir -pv build
        cd build

        ../configure --prefix=/usr \
                     --host=$LFS_TGT

        make -j $NUMTHREADS
        make DESTDIR=$LFS install

        mv -v $LFS/usr/bin/gzip $LFS/bin

        echo "Finished gzip installation"
        sync
        sleep $SLEEPTIME
)

#####################
# Make Installation #
#####################

(
        exec > >(sed 's/^/make: /')
        exec 2> >(sed 's/^/ERROR: /')
        echo "Starting make compilation"

        cd $LFS/sources/
        cd $(ls -d1 */ | grep -E "^make")

        mkdir -pv build
        cd build

        ../configure --prefix=/usr \
                     --without-guile \
                     --host=$LFS_TGT \
                     --build=$(../build-aux/config.guess)

        make -j $NUMTHREADS
        make DESTDIR=$LFS install

        echo "Finished make installation"
        sync
        sleep $SLEEPTIME
)

######################
# Patch Installation #
######################

(
        exec > >(sed 's/^/patch: /')
        exec 2> >(sed 's/^/ERROR: /')
        echo "Starting patch compilation"

        cd $LFS/sources/
        cd $(ls -d1 */ | grep patch)

        mkdir -pv build
        cd build

        ../configure --prefix=/usr \
                     --host=$LFS_TGT \
                     --build=$(../build-aux/config.guess)

        make -j $NUMTHREADS
        make DESTDIR=$LFS install

        echo "Finished tar installation"
        sync
        sleep $SLEEPTIME
)

####################
# Sed Installation #
####################

(
        exec > >(sed 's/^/sed: /')
        exec 2> >(sed 's/^/ERROR: /')
        echo "Starting sed compilation"

        cd $LFS/sources/
        cd $(ls -d1 */ | grep sed)

        mkdir -pv build
        cd build


        ../configure

        make -j $NUMTHREADS
        make DESTDIR=$LFS install

        echo "Finished tar installation"
        sync
        sleep $SLEEPTIME
)

####################
# Tar Installation #
####################

(
        exec > >(sed 's/^/tar: /')
        exec 2> >(sed 's/^/ERROR: /')
        echo "Starting tar compilation"

        cd $LFS/sources/
        cd $(ls -d1 */ | grep tar)

        mkdir -pv build
        cd build

        ../configure --prefix=/usr \
                     --host=$LFS_TGT \
                     --build=$(../build-aux/config.guess) \
                     --bindir=/bin

        make -j $NUMTHREADS
        make DESTDIR=$LFS install

        echo "Finished tar installation"
        sync
        sleep $SLEEPTIME
)

###################
# Xz Installation #
###################

(
        exec > >(sed 's/^/xz: /')
        exec 2> >(sed 's/^/ERROR: /')
        echo "Starting xz compilation"

        cd $LFS/sources/
        cd $(ls -d1 */ | grep xz)

        mkdir -pv build
        cd build

        docdir=$(ls -d1 $LFS/sources/*/ | grep -oE "xz-.*")
        docdir=${docdir::-1}

        ../configure --prefix=/usr \
                     --host=$LFS_TGT \
                     --build=$(build-aux/config.guess) \
                     --disable-static \
                     --docdir=/usr/share/doc/$docdir

        make -j $NUMTHREADS
        make DESTDIR=$LFS install

        mv -v $LFS/usr/bin/{lzma,unlzma,lzcat,xz,unxz,xzcat}  $LFS/bin
        mv -v $LFS/usr/lib/liblzma.so.*                       $LFS/lib
        ln -svf ../../lib/$(readlink $LFS/usr/lib/liblzma.so) $LFS/usr/lib/liblzma.so

        echo "Finished xz installation"
        sync
        sleep $SLEEPTIME
)

##################################
# Binutils Installation - Pass 2 #
##################################

(
        exec > >(sed 's/^/binutils: /')
        exec 2> >(sed 's/^/ERROR: /')
        echo "Starting binutils compilation"

        cd $LFS/sources/
        cd $(ls -d1 */ | grep binutils)

        mkdir -pv build-pass-2
        cd build-pass-2

        ../configure --prefix=/usr \
                     --build=$(../config.guess) \
                     --host=$LFS_TGT \
                     --disable-nls \
                     --enable-shared \
                     --disable-werror \
                     --enable-64-bit-bfd

        make -j $NUMTHREADS
        make DESTDIR=$LFS install

        echo "Finished binutils installation"
        sync
        sleep $SLEEPTIME
)

#############################
# GCC Installation - Pass 2 #
#############################

(
        exec > >(sed 's/^/gcc: /')
        exec 2> >(sed 's/^/ERROR: /')
        echo "Starting gcc pass 2 compilation"

        cd $LFS/sources/
        cd $(ls -d1 */ | grep gcc)

        # Apply patch
        patch -Np1 -i ../$(cd ../; ls -1 | grep -E "gcc-.*-rpi4-cpu-default.patch") || true

        # untar and move mpfr, gmp, and mpc to build dir
        echo "Moving mpfr, gmp, and mpc to build dir"
        cp -rfT ../$(cd ../; ls -d1 */ | grep mpfr) mpfr/
        cp -rfT ../$(cd ../; ls -d1 */ | grep gmp) gmp/
        cp -rfT ../$(cd ../; ls -d1 */ | grep mpc) mpc/

        mkdir -pv build-pass-2
        cd build-pass-2

        mkdir -pv $LFS_TGT/libgcc
        ln -s ../../../libgcc/gthr-posix.h $LFS_TGT/libgcc/gthr-default.h

        ../configure --prefix=/usr \
                     --host=$LFS_TGT \
                     --build=$(../config.guess) \
                     CC_FOR_TARGET=$LFS_TGT-gcc \
                     --with-build-sysroot=$LFS \
                     --enable-initfini-array \
                     --disable-nls \
                     --disable-multilib \
                     --disable-decimal-float \
                     --disable-libatomic \
                     --disable-libgomp \
                     --disable-libquadmath \
                     --disable-libssp \
                     --disable-libvtv \
                     --disable-libstdcxx \
                     --enable-languages=c,c++

        make -j $NUMTHREADS
        make DESTDIR=$LFS install

        ln -sv gcc $LFS/usr/bin/cc

        echo "Finished gcc installation"
        sync
        sleep $SLEEPTIME
)

###############################################################################

)








