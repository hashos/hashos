#!/bin/bash
# Creates all directories needed and downloads sources
time ( # just time how long the script takes to run and print it once done

###############################################################################

if [[ -z $LFS ]]; then
        echo "Load the env!"
        exit
fi

download_flag=""

while getopts 'd' flag; do
        case "${flag}" in
                d) download_flag="true" ;;
        esac
done

mkdir -pv $LFS/sources
chmod -v a+wt $LFS/sources

mkdir -pv $LFS/{bin,etc,lib,sbin,usr,var}
mkdir -pv $LFS/lib64
mkdir -pv $LFS/tools

if [ ! -z $download_flag ]; then 
        echo "downloading packages"
        
        (
                cd $LFS/sources
                curl -LO http://hashfastr.com/build-mirror/wget-list
		cat wget-list | xargs -I {} bash -c 'echo "{}" && curl --retry 1000 --retry-delay 15 -LO "{}"'
                rm wget-list
        )
fi

echo "Untaring all source files"
(cd $LFS/sources; for a in `ls -1 | grep -E "\.tar"`; do echo "$a" && tar --checkpoint=1000 -xf $a; done)

###############################################################################

)
