#!/bin/bash

time ( # just time how long the script takes to run and print it once done

###############################################################################

# if an error occurs, exit the script
set -e
NUMTHREADS=4
SLEEPTIME=10

# colors for output
RED='\033[0;31m'
NC='\033[0m'

if [[ -z $LFS ]]; then
        echo "Load the env!"
        exit
fi

#####################
# Binutils - Pass 1 #
#####################
(
        exec > >(sed 's/^/binutils - Pass 1: /')
        exec 2> >(sed 's/^/ERROR: /')
        echo "Starting binutils compilation"
 
        cd $LFS/sources/
        cd $(ls -d1 */ | grep binutils)
    
        mkdir -pv build-pass-1
        cd build-pass-1
    
        ../configure --prefix=$LFS/tools \
                --with-sysroot=$LFS \
                --target=$LFS_TGT \
                --disable-nls \
                --disable-werror

        make -j $NUMTHREADS
        make install
        
        echo "Completed binutils compilation"
        sync
        sleep $SLEEPTIME
)

################
# GCC - Pass 1 #
################
(
        exec > >(sed 's/^/GCC - Pass 1: /')
        exec 2> >(sed 's/^/ERROR: /' >&2)
        echo "Starting GCC compilation"
        
        cd $LFS/sources/
        cd $(ls -d1 */ | grep gcc)

        # Apply patch
        patch -Np1 -i ../$(cd ../; ls -1 | grep -E "gcc-.*-rpi4-cpu-default.patch") || true

        # untar and move mpfr, gmp, and mpc to build dir
        echo "Moving mpfr, gmp, and mpc to build dir"
        cp -rfT ../$(cd ../; ls -d1 */ | grep mpfr) mpfr/
        cp -rfT ../$(cd ../; ls -d1 */ | grep gmp) gmp/
        cp -rfT ../$(cd ../; ls -d1 */ | grep mpc) mpc/

        mkdir -pv build-pass-1
        cd build-pass-1

        ../configure \
                --target=$LFS_TGT \
                --prefix=$LFS/tools \
                --with-glibc-version=2.11 \
                --with-sysroot=$LFS \
                --with-newlib \
                --without-headers \
                --enable-initfini-array \
                --disable-nls \
                --disable-shared \
                --disable-multilib \
                --disable-decimal-float \
                --disable-threads \
                --disable-libatomic \
                --disable-libgomp \
                --disable-libquadmath \
                --disable-libssp \
                --disable-libvtv \
                --disable-libstdcxx \
                --enable-languages=c,c++

        make -j $NUMTHREADS
        make install

        cd ..
        cat gcc/limitx.h gcc/glimits.h gcc/limity.h > `dirname $($LFS_TGT-gcc -print-libgcc-file-name)`/install-tools/include/limits.h

        echo "Completed GCC compilation"
        sync
        sleep $SLEEPTIME
)

#####################
# Linux API Headers #
#####################
(
        exec > >(sed 's/^/Linux Headers: /')
        exec 2> >(sed 's/^/ERROR: /' >&2)
        echo "Started compilation of Linux API headers"

        cd $LFS/sources
        cd $(ls -d1 */ | grep -E "^linux")

        make mrproper
        make headers

        find usr/include -name '.*' -delete
        rm usr/include/Makefile
        cp -rv usr/include $LFS/usr

        echo "Completed Linux API headers compilation"
        sync
        sleep $SLEEPTIME
)

#########
# glibc #
#########
(
        exec > >(sed 's/^/glibc: /')
        exec 2> >(sed 's/^/ERROR: /')
        echo "Started glibc compilation"

        cd $LFS/sources
        cd $(ls -d1 */ | grep glibc)

        case $(uname -m) in
                aarch64)
                        ln -sfv ../../lib/ld-linux-aarch64.so.2 $LFS/lib64
                        ln -sfv ../../lib/ld-linux-aarch64.so.2 $LFS/lib64/ld-lsb-aarch64.so.3
                ;;

                *)
                        echo "NOT SUPPORTED ARCH, FIX THE SCRIPT STUPID" 2>&2
                ;;
        esac

        # apply a patch
        patch -Np1 -i ../$(cd ../; ls -1 | grep -E "glibc-.*\.patch")

        mkdir -pv build
        cd build

        ../configure \
                --prefix=/usr \
                --host=$LFS_TGT \
                --build=$(../scripts/config.guess) \
                --enable-kernel=3.2 \
                --with-headers=$LFS/usr/include \
                libc_cv_slibdir=/lib

        make -j $NUMTHREADS
        make DESTDIR=$LFS install

        echo "Finalizing limits.h header..."
        $LFS/tools/libexec/gcc/$LFS_TGT/10.2.0/install-tools/mkheaders
        echo "Finished finalizing limits.h header!"

        # probably don't need this, maybe in the future if it cannot find the right linker
        # since our linker is named after the arch
        # ln -sv ld-2.32.so $LFS/lib/ld-linux.so.3

        echo "Completed glibc compilation"
        sync
        sleep $SLEEPTIME
)

#################
# Sanity Check! #
#################

(
        exec > >(sed 's/^/Sanity Check: /')
        exec 2> >(sed 's/^/ERROR: /')
        echo "Running sanity check..."
        echo 'int main(){}' > dummy.c
        $LFS_TGT-gcc dummy.c
        if [[ "$(readelf -l a.out | grep '/ld-linux')" != *"Requesting program interpreter"* ]]; then
                echo "Sanity check failed!"
                rm dummy.c
                exit
        fi
        rm dummy.c a.out
        echo "Sanity check passed!"
)

####################
# libstdc++ pass 1 #
####################

(
        exec > >(sed 's/^/libstdc++: /')
        exec 2> >(sed 's/^/ERROR: /')
        echo "Starting libstdc++ compilation"
        
        cd $LFS/sources/
        cd $(ls -d1 */ | grep gcc)

        mkdir -pv build-libstdc++
        cd build-libstdc++

        ../libstdc++-v3/configure \
                --host=$LFS_TGT \
                --build=$(../config.guess) \
                --prefix=/usr \
                --disable-multilib \
                --disable-nls \
                --disable-libstdcxx-pch \
                --with-gxx-include-dir=/tools/$LFS_TGT/include/c++/10.2.0

        make -j $NUMTHREADS
        make DESTDIR=$LFS install

        echo "Completed compiling libstdc++!"
        sync
        sleep $SLEEPTIME
)

###############################################################################

)
